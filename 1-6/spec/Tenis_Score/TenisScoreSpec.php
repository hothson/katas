<?php

namespace spec\Tenis_Score;

use Tenis_Score\TenisScore;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;
use Tenis_Score\Player;	

class TenisScoreSpec extends ObjectBehavior
{
	protected $john;
	
	protected $jane;

	function let()
	{
		$this->john = new Player('John', 0);
		$this->jane = new Player('Jane', 0);
		$this->beConstructedWith($this->john, $this->jane);//What is this
	}
    function it_score_a_0_0_game()
    {
    	$this->score()->shouldReturn('Love-All');
    }

    function it_score_a_1_0_game()
    {
    	$this->john->earnPoints(1);
    	$this->score()->shouldReturn('Fifteen-Love');
    }

    function it_score_a_2_0_game()
    {
    	$this->john->earnPoints(2);
    	$this->score()->shouldReturn('Thirty-Love');
    }

    function it_score_a_3_0_game()
    {
    	$this->john->earnPoints(3);
    	$this->score()->shouldReturn('Forty-Love');
    }

    function it_score_a_4_0_game()
    {
    	$this->john->earnPoints(4);
    	$this->score()->shouldReturn('Win for John');
    }

    function it_score_a_4_2_game()
    {
    	$this->john->earnPoints(2);
    	$this->jane->earnPoints(4);
    	$this->score()->shouldReturn('Win for Jane');
    }


    function it_score_a_3_4_game()
    {
    	$this->john->earnPoints(3);
    	$this->jane->earnPoints(4);
    	$this->score()->shouldReturn('Advantage for Jane');
    }


    function it_score_a_3_3_game()
    {
    	$this->john->earnPoints(4);
    	$this->jane->earnPoints(4);
    	$this->score()->shouldReturn('Deuce');
    }
}

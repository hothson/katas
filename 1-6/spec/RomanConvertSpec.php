<?php

namespace spec;

use RomanConvert;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class RomanConvertSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType(RomanConvert::class);
    }
}

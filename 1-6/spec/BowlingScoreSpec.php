<?php

namespace spec;

use BowlingScore;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BowlingScoreSpec extends ObjectBehavior
{
    function it_scores_zero_for_a_worst_game()
    {
        for ($i = 0; $i < 20 ; $i++)
        { 
            $this->roll(0);
        }

        $this->score()->shouldBe(0); 
    }

    function it_scores_some_of_knocked_down_pins_for_a_game()
    {
        $this->roll(2);
        $this->roll(3);
        $this->rollTime(18, 0);
        $this->score()->shouldBe(5);
    }

    function it_scores_every_spare_for_a_game()
    {
        $this->roll(2);
        $this->roll(8);
        $this->roll(2);
        $this->rollTime(17, 0);
        $this->score()->shouldBe(14);
    }

    function it_scores_a_perfect_game()
    {
        $this->rollTime(12,10);

        $this->score()->shouldBe(300);
    }

    function it_scores_11_consecutive_strike_for_a_game()
    {
        $this->rollTime(11, 10);
        $this->rollTime(1, 0);

        $this->score()->shouldBe(290);
    }

    public function rollTime($time, $pin)
    {
        for ($i = 0; $i < $time ; $i++)
        { 
            $this->roll($pin);
        }
    }
        
}

<?php

namespace spec\String_Caculator;

use string_caculator\StringCaculator;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class StringCaculatorSpec extends ObjectBehavior
{
    function it_convert_empty_string_into_zero()
    {
    	$this->add("")->shouldEqual(0);
    }

    function it_find_the_sum_of_one_number()
    {
    	$this->add("5")->shouldEqual(5);
    }

    function it_find_the_sum_of_two_number()
    {
    	$this->add("4,3")->shouldEqual(7);
    }

    function it_dissallow_nagative_number()
    {
    	$this->shouldThrow(new \InvalidArgumentException('Invalid Number Provided: -2'))->duringAdd('1,-2');
    }

    function it_alow_new_line_delimeter()
    {
        $this->add('1,2\n3')->shouldEqual(6);
    }

    function it_ignores_any_number_that_is_one_thousand_or_bigger()
    {
        $this->add('1,2, 1000')->shouldEqual(3);
    }
}

<?php

namespace string_caculator;

class StringCaculator
{
	private function guardAgainstInvalidNumber($number)
	{
		throw new \InvalidArgumentException('Invalid Number Provided: '.$number);
	}

	private function parseNumbers($numbers)
	{
		return preg_split ('/\s*(,|\\\n)\s*/', $numbers);
	}

	public function add($numbers)
	{
		$numbersArr = $this->parseNumbers($numbers);

		$result = 0;
		foreach ($numbersArr as $number) {
			if ($number < 0) {
				$this->guardAgainstInvalidNumber($number);
			}

			if ($number >= 1000) {
				continue;	
			} 
				
			$result += (int) $number;
		}

		return $result;
	}
}

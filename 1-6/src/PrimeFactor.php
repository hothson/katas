<?php

class PrimeFactor
{
	function generate($number, $result = [])
	{
		if ($number == 0 || $number == 1) {
			return $result;
		}

		for ($i = 2; $i <=$number; $i++) {
			if ($number % $i == 0) {
				$result[] = $i;
				return $this->generate($number/$i, $result);
			}
		}
	}
}
$primeFactor = new PrimeFactor();
$result = $primeFactor->generate(4);
print_r($result);
// 15 > 3, 5
// 16 > 2,2,2,2
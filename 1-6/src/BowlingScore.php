<?php

class BowlingScore
{   
    protected $rolls = [];

    public function roll($pin)
    {
        $this->rolls[] = $pin;
    }

    private function isStrike($frame)
    {
        return $frame['status'] == "strike";
    }

    private function isSpare($frame)
    {
        return $frame['status'] == "spare";
    }

    private function nextNotStrike($frames, $key)
    {
        $frames[$key + 1][0] != 10;
    }

    private function nextIsStrike($frames, $key)
    {
        return isset($frames[$key + 1][0]) && isset($frames[$key + 2][0]) && $frames[$key + 1][0] == 10;
    }

    private function getScoreAtFrameNext($frames, $key, $offset)
    {
        if (isset($frames[$key][$offset])) {
            return $frames[$key][$offset];
        }

        return 0;
    }

    public function frameDivide()
    {
        $frames = [];
        for ($i = 0; $i < count($this->rolls);) {
            if ($this->rolls[$i] == 10) {
                $frames[] = [
                    0 => $this->rolls[$i],
                    1 => 0,
                    'status' => 'strike',
                ];
                $i++;
            } elseif (isset($this->rolls[$i + 1]) && ($this->rolls[$i] + $this->rolls[$i + 1] == 10)) {
                $frames[] = [
                    0 => $this->rolls[$i],
                    1 => $this->rolls[$i + 1],
                    'status' => 'spare',
                ];
                $i += 2;
            } else {
                if (isset($this->rolls[$i + 1])) {
                    $roll = $this->rolls[$i + 1];
                } else {
                    $roll = 0;
                }

                $frames[] = [
                    0 => $this->rolls[$i],
                    1 => $roll,
                    'status' => 'normal',
                ];
                $i += 2;
            }
        }
        return $frames;
    }

    public function score()
    {
        $frames = $this->frameDivide();
        $score = 0;
        $count = 0;
        foreach ($frames as $key => $frame) {
            if ($key < 10) {
                $score += $frame[0] + $frame[1];
                if ($this->isSpare($frame)) {
                    $score += $this->getScoreAtFrameNext($frames, $key + 1, 0);
                } elseif ($this->isStrike($frame) && $this->nextNotStrike($frames, $key)) {
                    $score += $this->getScoreAtFrameNext($frames, $key + 1, 0) + $this->getScoreAtFrameNext($frames, $key + 1, 1);
                } elseif ($this->isStrike($frame) && $this->nextIsStrike($frames, $key)) {
                    $score +=$this->getScoreAtFrameNext($frames, $key + 1, 0) + $this->getScoreAtFrameNext($frames, $key + 2, 0);
                }
            }
        }
        return $score;
    }
}
